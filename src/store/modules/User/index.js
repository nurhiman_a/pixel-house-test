import actions from './actions'
import mutations from './mutations'

const state = {
  userList: [],
  selectedId: null
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
