import Vue from 'vue'
import axios from 'axios'

const baseUrl = 'https://jsonplaceholder.typicode.com'

Vue.use(axios)

const getListUser = ({ commit }) => {
  return new Promise((resolve, reject) => {
    axios.get(baseUrl + '/users')
      .then(response => {
        commit('SET_LIST', response.data)
        resolve(response)
      }, error => {
        reject(error)
      })
  })
}

const postUser = ({ commit }, userObj) => {
  return new Promise((resolve, reject) => {
    axios.post(baseUrl + '/users', userObj, {
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    })
      .then(response => {
        resolve(response)
        commit('PUSH_USER', response.data)
      }, error => {
        reject(error)
      })
  })
}

const editUser = ({ state, commit }, userObj) => {
  return new Promise((resolve, reject) => {
    axios.put(baseUrl + '/users/' + state.selectedId, userObj, {
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    })
      .then(response => {
        resolve(response)
        commit('EDIT_USER', response.data)
      }, error => {
        reject(error)
      })
  })
}

const deleteUser = ({ state, commit }) => {
  return new Promise((resolve, reject) => {
    axios.delete(baseUrl + '/users/' + state.selectedId)
      .then(response => {
        commit('DELETE_USER', state.selectedId)
        resolve(response)
      }, error => {
        reject(error)
      })
  })
}

export default {
  getListUser,
  postUser,
  editUser,
  deleteUser
}
