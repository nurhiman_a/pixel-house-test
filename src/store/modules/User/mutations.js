const SET_LIST = (state, payload) => {
  state.userList = payload
}

const PUSH_USER = (state, payload) => {
  state.userList.push(payload)
}

const SET_SELECTED_ID = (state, payload) => {
  state.selectedId = payload
}

const EDIT_USER = (state, payload) => {
  state.userList.forEach(element => {
    if (element.id == payload.id) {
      element.name = payload.name
      element.username = payload.username
      element.email = payload.email
      element.phone = payload.phone
    }
  })
}

const DELETE_USER = (state, payload) => {
  state.userList.forEach((element, index) => {
    if (element.id == payload) {
      state.userList.splice(index, 1)
    }
  })
}

export default {
  SET_LIST,
  PUSH_USER,
  SET_SELECTED_ID,
  EDIT_USER,
  DELETE_USER
}
