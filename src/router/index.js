import Vue from 'vue'
import Router from 'vue-router'

import HomeLayout from '@/layouts/Home'

const DashboardPage = () => import('@/views/Dashboard')

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      redirect: '/dashboard',
      component: HomeLayout,
      children: [
        {
          path: '/dashboard',
          name: 'Dashboard',
          component: DashboardPage
        }
      ]
    }
  ]
})
